library(ggplot2)
library(dplyr)
library(viridis)

data_dir <- '~/Mapping/hg38/data/'
plot_dir <- '~/Mapping/paper/plots/real_bars/'
SAME_THRESHOLD <- 25
bin_thresholds1 <- c(0, 5, 10, 20, 30, 255)
bin_thresholds2 <- c(0, 5, 10, 20, 30, 255)

load_realignment <- function(filename_prefix) {
  realignment <- read.csv(gzfile(paste0(data_dir, filename_prefix, '.realignment.csv.gz')),
    sep='\t', comment.char='#', stringsAsFactors=F)
  realignment$aln_loc <- factor(ifelse(realignment$prev_overlap >= SAME_THRESHOLD,
                                       'Same', 'Different'), levels=c('Same', 'Different'))
  realignment$bin_before <- bins_from_mapq(realignment$prev_mapq, bin_thresholds1)
  realignment$bin_after <- bins_from_mapq(realignment$mapq, bin_thresholds2)
  realignment
}

load_bin_counts <- function(...) {
  realignment <- load_realignment(...)
  bin_counts <- realignment %>% count(bin_before, bin_after, aln_loc)
  bin_counts$perc <- 100 * bin_counts$n / nrow(realignment)
  bin_counts
}

get_tech_long <- function(tech) {
  if (tech == 'ccs') {
    return('PacBio CCS')
  } else if (tech == 'clr') {
    return('PacBio CLR')
  } else if (tech == 'ont') {
    return('ONT')
  } else {
    return(tech)
  }
}

bins_from_mapq <- function(mapq, bin_thresholds) {
  n_bins <- length(bin_thresholds) - 1
  bins <- sprintf('%d-%d', bin_thresholds[1:n_bins], bin_thresholds[2:(n_bins + 1)] - 1)
  bin_ids <- sapply(mapq, function(x) {
    for (i in 1:n_bins) {
      if (x < bin_thresholds[i + 1]) {
        return(i)
      }
    }
  })
  factor(bins[bin_ids], levels=bins)
}

res <- data.frame()
experiments <- t(matrix(c(
  'HG002', 'clr',
  'HG003', 'clr',
  'HG004', 'clr',
  'HG002', 'ccs',
  'HG001', 'ccs',
  'HG005', 'ccs',
  'HG001', 'ont',
  'HG002', 'ont'
  ), nrow=2))
for (i in 1:nrow(experiments)) {
  sample <- experiments[i, 1]
  tech <- experiments[i, 2]
  tech_long <- get_tech_long(tech)
  cat(sprintf('[%d / %d] Drawing %s: %s\n', i, nrow(experiments), sample, tech_long))
  bin_counts <- load_bin_counts(sprintf('%s/analysis/%s.minimap2', sample, tech))
  bin_counts$flag <- sprintf('%s: %s', sample, tech_long)
  res <- rbind(res, bin_counts)

  g <- ggplot(bin_counts) +
    geom_bar(aes(aln_loc, perc, fill=forcats::fct_rev(bin_after)),
             color='black', size=.1,
             stat='identity', position='stack') +
    scale_x_discrete('Alignment location') +
    scale_y_continuous('Percentage of reads',
                       limits=c(0, 57), breaks=seq(0, 50, 10)) +
    scale_fill_viridis('MM2 + DuploMap\nmapping quality', discrete=T, direction=-1,
                       guide=guide_legend(reverse=TRUE)) +
    facet_wrap(~ bin_before, nrow=1) +
    labs(title=sprintf('(%s) %s: %s', letters[i], sample, tech_long),
         subtitle='Minimap2 mapping quality') +
    theme_bw() +
    theme(plot.subtitle=element_text(hjust=.5))
  ggsave(sprintf('%s/%s.%s.%s.png', plot_dir, letters[i], sample, tech),
         g, width=8, height=6)
}


tmp <- load_bin_counts('HG002/analysis/clr.minimap2')
tmp$flag <- 'HG002: PacBio CLR'
tmp2 <- load_bin_counts('HG002/analysis/ccs.minimap2')
tmp2$flag <- 'HG002: PacBio CCS'
bin_counts <- rbind(tmp, tmp2)

margins <- unit(c(0.005, 0.0, 0.005, 0.005), 'npc')
ggplot(bin_counts) +
  geom_bar(aes(aln_loc, perc, fill=forcats::fct_rev(bin_after)),
           color='black', size=.1,
           stat='identity', position='stack') +
  #geom_text(aes(aln_loc, perc,
                #label=ifelse(perc > 2, sprintf('%.1f', perc), '')),
                #color=as.numeric(bin_after) > 2),
            #position=position_stack(vjust=.5)) +
  scale_x_discrete('Alignment location') +
  scale_y_continuous('Percentage of reads', breaks=seq(0, 50, 10)) +
  scale_fill_viridis('MM2 + DuploMap\nmapping quality', discrete=T, direction=-1,
                     guide=guide_legend(reverse=TRUE)) +
  facet_grid(flag ~ bin_before) +
  labs(subtitle='Minimap2 mapping quality') +
  theme_bw() +
  theme(plot.subtitle=element_text(hjust=.5),
        plot.margin=margins)
ggsave(sprintf('%s/main.png', plot_dir), width=10, height=8, scale=.9)

res1 <- res[res$flag != 'HG002: PacBio CLR' & res$flag != 'HG002: PacBio CCS',]

ggplot(res1) +
  geom_bar(aes(aln_loc, perc, fill=forcats::fct_rev(bin_after)),
           color='black', size=.1,
           stat='identity', position='stack') +
  scale_x_discrete('Alignment location') +
  scale_y_continuous('Percentage of reads', breaks=seq(0, 50, 10)) +
  scale_fill_viridis('MM2 + DuploMap\nmapping quality', discrete=T, direction=-1,
                     guide=guide_legend(reverse=TRUE)) +
  facet_grid(flag ~ bin_before) +
  labs(subtitle='Minimap2 mapping quality') +
  theme_bw() +
  theme(plot.subtitle=element_text(hjust=.5),
        plot.margin=margins)
ggsave(sprintf('%s/supp.png', plot_dir), width=8, height=10, scale=1.2)
