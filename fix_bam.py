#!/usr/bin/env python3

import sys
import pysam
import subprocess


def main():
    filename = sys.argv[1]
    out_filename = filename[:-3] + 'new.bam'
    inp = pysam.AlignmentFile(filename)
    outp = pysam.AlignmentFile(out_filename, 'wb', template=inp)
    changed_confl = 0
    changed_unknown = 0

    for read in inp:
        if read.flag & 0x904 or not read.has_tag('re'):
            outp.write(read)
            continue
        re_tag = read.get_tag('re').split(',')
        reason = re_tag[-1]
        if reason == 'confl':
            read.mapping_quality = min(read.mapping_quality, 5)
            changed_confl += 1
        elif reason == 'unknown_ref':
            read.mapping_quality = int(read.get_tag('om'))
            changed_unknown += 1
        outp.write(read)

    inp.close()
    outp.close()
    sys.stderr.write('%s. Changed: conflicting %d, unknown reference %d\n' % (filename, changed_confl, changed_unknown))
    subprocess.run(['samtools', 'index', out_filename], check=True)


if __name__ == '__main__':
    main()

