#!/usr/bin/env python3

import argparse
import tqdm
import collections
import pysam
import itertools
import subprocess
import os.path
import sys

import common
import count_psvs


def load_basecalls(var_file):
    res = set()
    for record in var_file:
        if not record.alts:
            continue
        for alt in record.alts:
            res.add((record.chrom, record.start, record.ref, record.alts[0]))
    return res


def generate_possible_chroms(chroms):
    if chroms == 'all':
        return None

    suffixes = list(range(1, 23))
    if chroms == '1-Y':
        suffixes.extend('XY')
    else:
        assert chroms == '1-22'

    prefixes = ['', 'chr']
    return set(map('%s%s'.__mod__, itertools.product(prefixes, suffixes)))


def fetch_variants(var_file, regions_file, chroms):
    chroms_set = generate_possible_chroms(chroms)
    regions = []
    for line in regions_file:
        chrom, start, end = line.strip().split('\t')[:3]
        if chroms_set and chrom not in chroms_set:
            continue

        start = int(start)
        end = int(end)
        regions.append((chrom, start, end))
    return itertools.chain(*itertools.starmap(var_file.fetch, regions))


def compare_with_basecalls(variants, basecalls, qual, out_files):
    counter = collections.Counter()
    total = 0

    for variant in variants:
        if variant.filter.keys() != ['PASS'] or variant.qual < qual:
            continue
        total += 1
        for alt in variant.alts:
            if ((variant.chrom, variant.start, variant.ref, alt)) in basecalls:
                break
        else:
            out_files[1].write(variant)
            # No breaks occured.
            continue

        gt = count_psvs.gt_to_str(variant.samples[0]['GT'])
        counter[gt] += 1
        out_files[0].write(variant)
    return counter, total


def main():
    parser = argparse.ArgumentParser(
        description='Find variants that exactly overlap a basecall.',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -v <vcf> -b <vcf> -r <bed> [-o <dir>] [args]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-v', '--variants', metavar='<file>', required=True,
        help='Input indexed vcf.gz file with called variants.')
    io_args.add_argument('-b', '--basecalls', metavar='<file>', required=True,
        help='Input vcf file with basecalls.')
    io_args.add_argument('-r', '--regions', metavar='<file>', required=True,
        help='Input bed file with regions of intereset.')
    io_args.add_argument('-o', '--out-dir', metavar='<dir>', required=False,
        help='Optional: output directory.')

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('--chroms', choices=['all', '1-22', '1-Y'], default='1-22',
        help='Use subset of chromosomes: all; 1-22; or 1-22, X and Y.')
    opt_args.add_argument('--qual', type=float, metavar='<float>', default=30,
        help='Variant quality [%(default)s].')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    args = parser.parse_args()

    with pysam.VariantFile(args.basecalls) as inp:
        basecalls = load_basecalls(inp)

    with pysam.VariantFile(args.variants) as var_inp, common.open_possible_gzip(args.regions) as regions_inp:
        if args.out_dir:
            common.mkdir(args.out_dir)
            filenames = ['overlap.vcf.gz', 'no_overlap.vcf.gz']
            filenames = [os.path.join(args.out_dir, filename) for filename in filenames]
            out_files = [pysam.VariantFile(filename, 'wz', header=var_inp.header) for filename in filenames]
        else:
            filenames = None
            out_files = None

        var_it = fetch_variants(var_inp, regions_inp, args.chroms)
        counter, total = compare_with_basecalls(var_it, basecalls, args.qual, out_files)

    res = 'Total {:,} variants\n'.format(total)
    ov = sum(counter.values())
    res += 'Overlap basecalls: {:,} ({:4.1f}%)\n'.format(ov, 100 * ov / total)
    for gt, count in counter.most_common():
        res += '    {}: {:,} ({:4.1f}%)\n'.format(gt, count, 100 * count / total)
    print(res, end='')
    if args.out_dir:
        with open(os.path.join(args.out_dir, 'summary.txt'), 'w') as outp:
            outp.write('# %s\n' % ' '.join(sys.argv))
            outp.write(res)

    if filenames:
        for i in range(2):
            out_files[i].close()
            subprocess.run(['tabix', '-p', 'vcf', filenames[i]], check=True)


if __name__ == '__main__':
    main()
