#!/usr/bin/env python3

import argparse
import pysam
import os.path
import numpy as np
import sys

import common
from summarize_reads import Location
from count_psvs import gt_to_str


def load_positions(file):
    next(file)
    next(file)

    loc1 = Location(None, None, None)
    loc2 = Location(None, None, None)
    res = {}
    for line in file:
        chrom2, pos2, chrom1, pos1 = line.strip().split('\t')[:4]
        if pos1 == 'NA' or pos2 == 'NA':
            continue
        pos1_int = int(float(pos1))
        pos2 = int(pos2)
        if loc1.chrom is None:
            loc1.chrom = chrom1
            loc2.chrom = chrom2
            loc1.start = pos1_int - 1
            loc1.end = pos1_int
            loc2.start = pos2 - 1
            loc2.end = pos2
        else:
            assert loc1.chrom == chrom1
            assert loc2.chrom == chrom2
            loc1.start = min(loc1.start, pos1_int - 1)
            loc1.end = max(loc1.end, pos1_int)
            loc2.start = min(loc2.start, pos2 - 1)
            loc2.end = max(loc2.end, pos2)
        res[pos2] = pos1
    return res, loc1, loc2


def analyze_variants(flag, vcf_file, positions, locs, outp):
    for i, loc in enumerate(locs):
        for variant in vcf_file.fetch(loc.chrom, loc.start, loc.end):
            if variant.filter.keys() and variant.filter.keys() != ['PASS']:
                continue
            if variant.qual is not None and variant.qual < 30:
                continue
            pos1 = variant.pos if i == 0 else positions.get(variant.pos, 'NA')
            gt = gt_to_str(variant.samples[0]['GT']) if variant.samples else '.'
            outp.write('%s\t%s\t%d\t%s\n' % (flag, pos1, variant.pos, gt))


def calculate_coverage(bam_file, positions, locs, outp):
    for i, loc in enumerate(locs):
        high_cov = np.zeros(len(loc), dtype=np.int16)
        low_cov = np.zeros(len(loc), dtype=np.int16)
        for read in bam_file.fetch(loc.chrom, loc.start, loc.end):
            if read.is_unmapped:
                continue
            coverage = high_cov if read.mapping_quality >= 10 else low_cov
            coverage[max(0, read.reference_start - loc.start) : read.reference_end - loc.start] += 1
        window_size = common.adjust_window_size(len(loc), 100)

        for j in range(0, len(loc), window_size):
            pos_start = loc.start + j + 1
            pos_end = min(loc.end, loc.start + j + window_size)
            if i == 0:
                outp.write('%d\t%d\t' % (pos_start, pos_end))
            else:
                pos1_start = positions.get(pos_start, 'NA')
                pos1_end = positions.get(pos_end, 'NA')
                if pos1_start != 'NA' and pos1_end != 'NA' and float(pos1_start) > float(pos1_end):
                    pos1_start, pos1_end = pos1_end, pos1_start
                outp.write('%s\t%s\t' % (pos1_start, pos1_end))
            outp.write('%d\t%d\t%d\t%d\n' % (pos_start, pos_end, np.median(high_cov[j : j + window_size]),
                np.median(high_cov[j : j + window_size] + low_cov[j : j + window_size])))


def main():
    parser = argparse.ArgumentParser(
        description='Combine variants and coverage across two copies of a duplication.',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -p <csv> -v <flag=vcf> [<flag=vcf> ...] -b <bam> -o <dir> [args]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-p', '--positions', metavar='<file>', required=True,
        help='Input csv file with aligned positions within two copies.')
    io_args.add_argument('-v', '--variants', metavar='<flag=file>', nargs='+',
        help='Multiple vcf files in format flag=path.')
    io_args.add_argument('-b', '--bam', metavar='<file>', required=True,
        help='Input BAM file.')
    io_args.add_argument('-o', '--output', metavar='<dir>', required=True,
        help='Output directory.')

    opt_args = parser.add_argument_group('Optional arguments')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    args = parser.parse_args()

    common.mkdir(args.output)
    with common.open_possible_gzip(args.positions) as inp:
        positions, loc1, loc2 = load_positions(inp)

    with open(os.path.join(args.output, 'variants.csv'), 'w') as outp:
        outp.write('# %s\n' % ' '.join(sys.argv))
        outp.write('flag\tpos1\tpos2\tgt\n')
        for entry in args.variants:
            flag, path = entry.split('=', 1)
            with pysam.VariantFile(path) as vcf_file:
                analyze_variants(flag, vcf_file, positions, [loc1, loc2], outp)

    with open(os.path.join(args.output, 'coverage.csv'), 'w') as outp, pysam.AlignmentFile(args.bam) as bam_file:
        outp.write('# %s\n' % ' '.join(sys.argv))
        outp.write('pos1_start\tpos1_end\tpos2_start\tpos2_end\thigh_cov\tcov\n')
        calculate_coverage(bam_file, positions, [loc1, loc2], outp)


if __name__ == '__main__':
    main()
