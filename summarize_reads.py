#!/usr/bin/env python3

import argparse
import re
import tqdm
import pysam
import sys

import common


_LOCATION_PATTERN = re.compile(r'([^:]+):([0-9]+)-([0-9]+)')

class Location:
    def __init__(self, chrom, start, end):
        self.chrom = chrom
        self.start = start
        self.end = end

    def __str__(self):
        return '%s:%d-%d' % (self.chrom, self.start + 1, self.end)

    def __len__(self):
        return self.end - self.start

    def overlaps(self, other):
        return self.chrom == other.chrom and self.start < other.end and other.start < self.end

    def overlap_size(self, other):
        if not self.overlaps(other):
            return 0
        return min(self.end, other.end) - max(self.start, other.start)

    def overlap_perc(self, other):
        return self.overlap_size(other) / max(len(self), len(other)) * 100.0

    def covered_perc(self, aln_loc):
        return self.overlap_size(aln_loc) / len(self) * 100.0

    def outside_tails(self, aln_loc):
        if not self.overlaps(aln_loc):
            return 'NA,NA'
        left = max(0, self.start - aln_loc.start)
        right = max(0, aln_loc.end - self.end)
        return '%d,%d' % (left, right)

    @classmethod
    def parse(cls, string):
        m = re.match(_LOCATION_PATTERN, string)
        return cls(m.group(1), int(m.group(2)) - 1, int(m.group(3)))

    def copy(self):
        return Location(self.chrom, self.start, self.end)

    def merge(self, other):
        assert self.overlaps(other)
        return Location(self.chrom, min(self.start, other.start), max(self.end, other.end))

    def __lt__(self, other):
        return (self.chrom, self.start, self.end) < (other.chrom, other.start, other.end)


_NAME_PATTERN = re.compile(r'Span=([^:]+):([0-9]+)-([0-9]+)/.*_([0-9]+)$')

def parse_name(name):
    """
    Returns pair (location, read_length).
    """
    m = re.search(_NAME_PATTERN, name)
    location = Location(m.group(1), int(m.group(2)) - 1, int(m.group(3)))
    return location, int(m.group(4))


def analyze_read(read, outp):
    name = read.query_name
    true_loc, read_len = parse_name(name)
    outp.write('%s\t%d\t%s\t' % (name, read_len, true_loc))
    if read.is_unmapped:
        outp.write('*\t0\n')
        return

    aln_loc = Location(read.reference_name, read.reference_start, read.reference_end)
    outp.write('%s\t%d\t%.1f\t%s' % (aln_loc, read.mapping_quality,
        true_loc.covered_perc(aln_loc), true_loc.outside_tails(aln_loc)))
    if read.has_tag('re'):
        outp.write('\t%s' % read.get_tag('re'))
    outp.write('\n')


def main():
    parser = argparse.ArgumentParser(
        description='Summarize reads in BAM file',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <bam> -o <csv>')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input BAM file.')
    io_args.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output CSV file.')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    args = parser.parse_args()

    with pysam.AlignmentFile(args.input) as inp, common.open_possible_gzip(args.output, 'w') as outp:
        outp.write('# %s\n' % ' '.join(sys.argv))
        outp.write('name\tread_len\ttrue_loc\taln_loc\tmapq\ttrue_loc_cov\toutside_tails\textra\n')
        for read in tqdm.tqdm(inp):
            if read.flag & 0x900:
                continue
            analyze_read(read, outp)


if __name__ == '__main__':
    main()
