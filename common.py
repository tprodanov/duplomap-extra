import sys
from time import perf_counter
from datetime import timedelta
import gzip
import os
from Bio import bgzf
import numpy as np


class EmptyContextManager:
    def __enter__(self):
        return None

    def __exit__(self, exc_type, exc_value, traceback):
        pass


def open_possible_gzip(filename, mode='r', bgzip=False):
    if filename == '-' or filename is None:
        return sys.stdin if mode == 'r' else sys.stdout

    if filename.endswith('.gz'):
        if bgzip:
            return bgzf.open(filename, mode + 't')
        return gzip.open(filename, mode + 't')

    return open(filename, mode)


def open_possible_empty(filename, *args, **kwargs):
    if filename is not None:
        return open_possible_gzip(filename, *args, **kwargs)
    return EmptyContextManager()


def log(string, out=sys.stderr, flush=True):
    elapsed = str(timedelta(seconds=perf_counter() - log._timer_start))[:-5]
    out.write('%s  %s\n' % (elapsed, string))
    if flush:
        out.flush()

log._timer_start = perf_counter()


class NonOverlappingSet:
    """
    Stores non-overlapping intervals and allows for fast retrieval of overlapping intervals.
    """
    def __init__(self, starts, ends):
        assert len(starts) == len(ends)
        self._starts = np.array(starts)
        self._ends = np.array(ends)

    def select(self, start, end):
        start_ix = self._ends.searchsorted(start, side='right')
        end_ix = self._starts.searchsorted(end, side='left')
        return start_ix, end_ix

    def select_by_pos(self, pos):
        start_ix = self._ends.searchsorted(pos, side='right')
        if start_ix == len(self._starts):
            return None
        return start_ix

    @property
    def starts(self):
        return self._starts

    @property
    def ends(self):
        return self._ends

    def __len__(self):
        return len(self._starts)


def format_length(length):
    if length < 1000:
        return str(length)
    elif length < 1e6:
        return '%.2fkb' % (length / 1000)
    else:
        return '%.2fGb' % (length / 1e6)


def mkdir(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass


def adjust_window_size(length, window):
    n_windows = np.ceil(length / window)
    return int(np.ceil(length / n_windows))
