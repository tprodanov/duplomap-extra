#!/usr/bin/env python3

import re
import os
import pandas as pd
import numpy as np
import argparse
import operator

import common


def reformat_tool(tool):
    tool_l = tool.lower()
    if tool_l == 'minimap2':
        return 'Minimap2'
    elif tool_l == 'ngmlr':
        return 'NGMLR'
    elif tool_l == 'blasr':
        return 'BLASR'
    return tool


def reformat_tech(tech):
    tech_l = tech.lower()
    if tech_l == 'clr':
        return 'PacBio CLR'
    elif tech_l == 'ccs':
        return 'PacBio CCS'
    elif tech_l == 'ont':
        return 'ONT'
    return tech


def get_column_formats(rows):
    widths = []
    for row in rows:
        widths.extend([0] * (len(row) - len(widths)))
        for i, col in enumerate(row):
            widths[i] = max(widths[i], len(col))
    return ['%{}s'.format(width) for width in widths]


def add_dir(dir, same_threshold, rows):
    for filename in os.listdir(os.path.join(dir, 'analysis')):
        if not filename.endswith('realignment.csv.gz'):
            continue
        m = re.match(r'^([^.]+)\.([^._]+)\.realignment.csv.gz$', filename)
        if m is None:
            common.log('Cannot parse "%s"' % filename)
        tech = reformat_tech(m.group(1))
        tool = reformat_tool(m.group(2))
        sample = os.path.basename(dir)

        df = pd.read_csv(os.path.join(dir, 'analysis', filename), sep='\t', comment='#')
        nrow_perc = df.shape[0] / 100
        different = np.sum(df.prev_overlap < same_threshold)

        prev_mapq10 = np.sum(df.prev_mapq >= 10)
        prev_mapq20 = np.sum(df.prev_mapq >= 20)
        new_mapq10 = np.sum(df.mapq >= 10)
        new_mapq20 = np.sum(df.mapq >= 20)

        rows.append((sample, tech, '__', '%dk' % round(df.shape[0] / 1000), '__',
            '%.1f' % (prev_mapq10 / nrow_perc), '%.1f' % (prev_mapq20 / nrow_perc),
            '%+.1f' % ((new_mapq10 - prev_mapq10) / nrow_perc), '%+.1f' % ((new_mapq20 - prev_mapq20) / nrow_perc)))


def main():
    parser = argparse.ArgumentParser(
        description='Create latex table with information about realigned reads',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <dir>')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<dir>', default='.',
        help='Input directory. Searching for files\n'
            '<input>/*/analysis/{technology}.{aligner}.realignment.csv.gz.')

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('-s', '--same', metavar='<float>', type=float, default=90.0,
        help='Same alignment threshold [%(default)s].')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    print('Genome', 'Sequencing technology', 'Median coverage', 'Reads analyzed', 'Read length (N50)',
        'Mapq >= 10', 'Mapq >= 20', 'Mapq >= 10', 'Mapq >= 20', sep=' & ', end=' \\\\\n')
    rows = []

    for dir in os.listdir(args.input):
        dir = os.path.join(args.input, dir)
        if os.path.isdir(dir) and os.path.exists(os.path.join(dir, 'analysis')):
            add_dir(dir, args.same, rows)

    rows.sort(key=operator.itemgetter(1))
    formats = get_column_formats(rows)
    for row in rows:
        print(*(fmt % col for fmt, col in zip(formats, row)), sep=' & ', end=' \\\\\n')


if __name__ == '__main__':
    main()
