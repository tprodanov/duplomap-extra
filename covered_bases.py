#!/usr/bin/env python3

import pysam
import tqdm
import numpy as np
import sys
import argparse
import collections

import common
from summarize_reads import Location

MAPQ_RANGE = 61


def analyse_region(long_region, short_regions, aln_file, covered_bases, total_length, min_coverage, covered_bases_hist):
    start = long_region.start
    length = len(long_region)
    # Matrix MAPQ x LENGTH
    coverage = np.zeros((MAPQ_RANGE, length), dtype=np.int16)
    for read in aln_file.fetch(long_region.chrom, start, long_region.end):
        if read.flag & 3844:
            continue
        coverage[min(MAPQ_RANGE - 1, read.mapping_quality),
            max(0, read.reference_start - start) : read.reference_end - start] += 1

    for region, flag in short_regions:
        total_length[flag] += len(region)
        curr_covered_bases = covered_bases[flag]
        curr_hist = covered_bases_hist[flag] if covered_bases_hist is not None else None

        for i in range(region.start - start, region.end - start):
            cumul_cov = 0
            for mapq in reversed(range(MAPQ_RANGE)):
                cumul_cov += coverage[mapq, i]
                if cumul_cov >= min_coverage:
                    curr_covered_bases[:mapq + 1] += 1
                    break
            if curr_hist is not None:
                curr_hist[sum(coverage[:, i])] += 1


def merge_overlapping(regions):
    regions.sort()
    current = {}
    res = []
    for region, flag in regions:
        if flag not in current:
            current[flag] = region
        elif current[flag].overlaps(region):
            current[flag] = current[flag].merge(region)
        else:
            res.append((current[flag], flag))
            current[flag] = region

    for flag, region in current.items():
        res.append((region, flag))
    res.sort()
    return res


def join_nearby_regions(regions, max_dist=1000):
    curr_long = None
    curr_short = None
    # List of pairs (long_region, [short_regions]).
    res = []

    for region, flag in regions:
        if curr_long is None or (curr_long.chrom != region.chrom or curr_long.end + max_dist < region.start):
            if curr_short:
                res.append((curr_long, curr_short))
            curr_long = region.copy()
            curr_short = [(region, flag)]
        else:
            curr_long.end = region.end
            curr_short.append((region, flag))
    if curr_short:
        res.append((curr_long, curr_short))
    return res


def main():
    parser = argparse.ArgumentParser(
        description='Count number of bases covered by reads with different MAPQ.',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <bam> -r <regions> -o <csv> [args]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input BAM file.')
    io_args.add_argument('-r', '--regions', metavar='<file>', required=True,
        help='Input BED file with segmental duplications regions.')
    io_args.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output CSV file.')
    io_args.add_argument('-O', '--out-hist', metavar='<file>', required=False,
        help='Optional: Output CSV file with coverage histogram (reads with all MQ).')

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('-a', '--all-chroms', action='store_true',
        help='Use all chromosomes. By default, only chromosomes 1-22 are used.')
    opt_args.add_argument('-c', '--coverage', metavar='<int>', type=int, default=10,
        help='Minimal required coverage [%(default)s].')
    opt_args.add_argument('-g', '--gene-mode', action='store_true',
        help='Calculate coverage for each gene.\n'
            'Regions BED file should have five fields, last two: gene_name and region_type.\n'
            'Only regions with region_type == exon are analyzed.')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    if args.gene_mode:
        covered_bases = collections.defaultdict(lambda: np.zeros(MAPQ_RANGE, dtype=np.int32))
        total_length = collections.defaultdict(int)
        covered_bases_hist = collections.defaultdict(collections.Counter) if args.out_hist else None
    else:
        covered_bases = [np.zeros(MAPQ_RANGE, dtype=np.int32)]
        total_length = [0]
        covered_bases_hist = [collections.Counter()] if args.out_hist else None

    regions = []
    with common.open_possible_gzip(args.regions) as regions_inp:
        for line in tqdm.tqdm(regions_inp):
            if line.startswith('#'):
                continue
            line = line.strip().split('\t')
            chrom, start, end = line[:3]
            if args.gene_mode and line[4] != 'exon':
                continue

            if not args.all_chroms:
                chrom_num = chrom[3:] if chrom.startswith('chr') else chrom
                if not chrom_num.isdigit() or not (1 <= int(chrom_num) <= 22):
                    continue
            regions.append((Location(chrom, int(start), int(end)), line[3] if args.gene_mode else 0))

    common.log('Total %d regions' % len(regions))
    regions = merge_overlapping(regions)
    common.log('Total %d non-overlapping regions' % len(regions))
    regions = join_nearby_regions(regions)
    common.log('Joined into %d groups' % len(regions))

    with pysam.AlignmentFile(args.input) as aln_file, \
            common.open_possible_gzip(args.regions) as regions_inp, \
            common.open_possible_gzip(args.output, 'w') as outp, \
            common.open_possible_empty(args.out_hist, 'w') as outp_hist:
        for long_region, short_regions in tqdm.tqdm(regions):
            analyse_region(long_region, short_regions, aln_file, covered_bases, total_length, args.coverage,
                covered_bases_hist)

        outp.write('# %s\n' % ' '.join(sys.argv))
        if args.gene_mode:
            outp.write('gene\tmapq\tcovered_bases\ttotal_len\n')
            for gene, length in total_length.items():
                for mapq in range(MAPQ_RANGE):
                    outp.write('%s\t%d\t%d\t%d\n' % (gene, mapq, covered_bases[gene][mapq], length))
            if args.out_hist:
                outp_hist.write('# %s\n' % ' '.join(sys.argv))
                outp_hist.write('gene\tcoverage\tbases\ttotal_len\n')
                for gene, length in total_length.items():
                    for coverage, bases in sorted(covered_bases_hist[gene].items()):
                        outp_hist.write('%s\t%d\t%d\t%d\n' % (gene, coverage, bases, length))

        else:
            outp.write('# total length: %d\n' % total_length[0])
            outp.write('mapq\tcovered_bases\n')
            for mapq in range(MAPQ_RANGE):
                outp.write('%d\t%d\n' % (mapq, covered_bases[0][mapq]))
            if args.out_hist:
                outp_hist.write('# %s\n' % ' '.join(sys.argv))
                outp_hist.write('# total length: %d\n' % total_length[0])
                outp_hist.write('coverage\tbases\n')
                for coverage, bases in sorted(covered_bases_hist[0].items()):
                    outp_hist.write('%d\t%d\n' % (coverage, bases))


if __name__ == '__main__':
    main()
