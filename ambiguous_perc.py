#!/usr/bin/env python3

import argparse
import pysam

import common


def count_ambigous(inp):
    seen = set()
    skipped = 0
    amb = 0
    total = 0
    for variant in inp:
        key = (variant.chrom, variant.pos, len(variant.ref), variant.alts[0])
        if key in seen:
            skipped += 1
            continue
        else:
            seen.add(key)
        support = list(map(int, variant.samples[0]['sp']))
        total += sum(support)
        amb += support[-1]
    print('Skipped  {:12,} PSVs'.format(skipped))
    print('Analyzed {:12,} PSVs'.format(len(seen)))
    return amb, total


def main():
    parser = argparse.ArgumentParser(
        description='Count ambigous alignments percentage.',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <vcf> [args]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input VCF file with PSVs.')

    opt_args = parser.add_argument_group('Optional arguments')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    args = parser.parse_args()

    with pysam.VariantFile(args.input) as inp:
        amb, total = count_ambigous(inp)
        print('Ambigous: {:12,}'.format(amb))
        print('Total:    {:12,}'.format(total))
        print('    {:.2f}%'.format(amb * 100 / total))


if __name__ == '__main__':
    main()
