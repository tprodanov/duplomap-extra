#!/usr/bin/env python3

import argparse
import tqdm
import pysam
import mappy

import common
import summarize_reads

FORWARD_ONLY = 0x100000


def realign_read(name, seq, qual, ref, out, err_out):
    true_loc, read_len = summarize_reads.parse_name(name)
    ref_seq = ref.fetch(true_loc.chrom, true_loc.start, true_loc.end).lstrip('N')
    start_pos = true_loc.start + len(true_loc) - len(ref_seq)
    ref_seq = ref_seq.rstrip('N')
    if not ref_seq:
        err_out.write('%s: empty reference sequence\n' % name)
        return

    k = 11 if len(seq) > 500 else 7
    aligner = mappy.Aligner(seq=ref_seq, preset='map-pb', best_n=1, extra_flags=FORWARD_ONLY, k=k)
    was_aligned = False
    for hit in aligner.map(seq):
        was_aligned = True
        if hit.r_st >= 100:
            err_out.write('%s: alignment has large start clipping: %d\n' % (name, hit.r_st))
        elif hit.r_st >= 20:
            err_out.write('%s: alignment has start clipping: %d\n' % (name, hit.r_st))

        end_clip = len(ref_seq) - hit.r_en
        if end_clip >= 100:
            err_out.write('%s: alignment has large end clipping: %d\n' % (name, end_clip))
        elif end_clip >= 20:
            err_out.write('%s: alignment has end clipping: %d\n' % (name, end_clip))

        record = pysam.AlignedSegment(header=out.header)
        record.query_name = name
        record.query_sequence = seq
        record.query_qualities = [ord(q) - 33 for q in qual]
        record.reference_name = true_loc.chrom
        record.reference_start = start_pos + hit.r_st

        if hit.q_st:
            cigar = '%dS' % hit.q_st
        else:
            cigar = ''
        cigar += hit.cigar_str
        if hit.q_en < len(seq):
            cigar += '%dS' % (len(seq) - hit.q_en)
        record.cigarstring = cigar
        record.mapping_quality = hit.mapq
        record.set_tag('NM', hit.NM)

        if not hit.is_primary:
            record.is_supplementary = True
            err_out.write('%s: non-primary alignment\n' % name)
        elif hit.mapq < 10:
            err_out.write('%s: low MAPQ: %d\n' % (name, hit.mapq))
        out.write(record)

    if not was_aligned:
        err_out.write('%s: did not align! (ref len: %d)\n' % (name, len(ref_seq)))


def main():
    parser = argparse.ArgumentParser(
        description='Get true read alignments',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <fastq> -f <fasta> -o <bam>')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input FASTQ file.')
    io_args.add_argument('-f', '--fasta-ref', metavar='<file>', required=True,
        help='Input indexed FASTA reference.')
    io_args.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output BAM file.')
    io_args.add_argument('-e', '--err-out', metavar='<file>', required=True,
        help='Output log file with errors.')
    io_args.add_argument('-n', '--names', metavar='<file>',
        help='Optional: analyse only read with names in this list.')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    args = parser.parse_args()

    if args.names:
        with common.open_possible_gzip(args.names) as inp:
            names = set(map(str.strip, inp))
    else:
        names = None

    with common.open_possible_gzip(args.input) as inp, common.open_possible_gzip(args.err_out, 'w') as err_out, \
            pysam.FastaFile(args.fasta_ref) as ref:
        out = pysam.AlignmentFile(args.output, 'wb', reference_names=ref.references, reference_lengths=ref.lengths)
        for name in tqdm.tqdm(inp):
            name = name.lstrip('@').strip()
            seq = next(inp).strip()
            next(inp)
            qual = next(inp).strip()
            if names and name not in names:
                continue
            realign_read(name, seq, qual, ref, out, err_out)

        out.close()



if __name__ == '__main__':
    main()
