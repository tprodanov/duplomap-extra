#!/usr/bin/env python3

import argparse
import tqdm

import common


def main():
    parser = argparse.ArgumentParser(
        description='Extract subset of Fastq reads',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <fastq> -n <txt> -o <fastq>')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input FASTQ file.')
    io_args.add_argument('-n', '--names', metavar='<file>', required=True,
        help='Input text file with read names.')
    io_args.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output FASTQ file.')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    args = parser.parse_args()

    with common.open_possible_gzip(args.names) as inp:
        names = set(map(str.strip, inp))

    with common.open_possible_gzip(args.input) as inp, common.open_possible_gzip(args.output, 'w') as outp:
        for name in tqdm.tqdm(inp):
            if name.lstrip('@').strip() in names:
                outp.write(name)
                for _ in range(3):
                    outp.write(next(inp))
            else:
                for _ in range(3):
                    next(inp)


if __name__ == '__main__':
    main()
