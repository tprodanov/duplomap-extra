#!/usr/bin/env python3

import argparse
import pybedtools
import os
import shutil
import sys
import csv
import numpy as np

import common


def create_lt_files(length, similarities, allowed_chroms, faidx_filename, inp, out_dir):
    length_str = format_length(length)
    # Skip similarities[0] and reverse.
    similarities = similarities[:0:-1]
    filenames = [os.path.join(out_dir, '%s.all.bed' % length_str)]
    filenames.extend(os.path.join(out_dir, '%s.lt_%.1f.bed' % (length_str, 100 * x)) for x in similarities)
    assert len(filenames) == len(set(filenames))

    tmp_filenames = [filename + '.tmp' for filename in filenames]
    out_files = [open(filename, 'w') for filename in tmp_filenames]
    reader = csv.DictReader(inp, delimiter='\t')

    for row in reader:
        chrom = row['chrom']
        start = int(row['chromStart'])
        end = int(row['chromEnd'])
        if chrom not in allowed_chroms or end - start < length:
            continue
        seq_sim = float(row['fracMatch'])
        str_line = '%s\t%d\t%d\n' % (chrom, start, end)
        out_files[0].write(str_line)

        for i, sim_threshold in enumerate(similarities):
            if seq_sim < sim_threshold:
                out_files[i + 1].write(str_line)
            else:
                break

    for f in out_files:
        f.close()
    for tmp_filename, filename in zip(tmp_filenames, filenames):
        pybedtools.BedTool(tmp_filename).sort(faidx=faidx_filename).merge().moveto(filename)
        os.remove(tmp_filename)
    return filenames[::-1]


def create_bin_files(lt_filenames, bin_names, faidx_filename, out_dir):
    assert len(lt_filenames) == len(bin_names)
    filenames = [os.path.join(out_dir, '%s.bed' % bin) for bin in bin_names]
    shutil.copyfile(lt_filenames[0], filenames[0])

    for i in range(1, len(filenames)):
        lower = pybedtools.BedTool(lt_filenames[i - 1])
        upper = pybedtools.BedTool(lt_filenames[i])
        upper.subtract(lower).moveto(filenames[i])
    return filenames


def format_length(length):
    if length > 1000 and length % 1000 == 0:
        return '%dkb' % (length // 1000)
    return common.format_length(length)


def main():
    parser = argparse.ArgumentParser(
        description='Extract subset of segmental duplications.',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <csv> -f <faidx> -o <dir> [args]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input CSV file with segmental duplications.')
    io_args.add_argument('-f', '--faidx', metavar='<file>', required=True,
        help='Input genome FAIDX file.')
    io_args.add_argument('-o', '--output', metavar='<dir>', required=True,
        help='Output directory.')

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('-s', '--similarities', metavar='<float>', type=float, nargs='+',
        default=list(range(90, 100, 1)) + [99.5, 99.9],
        help='Sequence similarity thresholds [%(default)s].')
    opt_args.add_argument('-l', '--length', metavar='<int>', type=int, default=10000,
        help='Minimal segmental duplication length [%(default)s].')
    opt_args.add_argument('-k', '--keep', action='store_true',
        help='Do not clean output files.')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    common.mkdir(args.output)

    with open(args.faidx) as inp:
        allowed_chroms = { line.strip().split()[0] for line in inp }

    length = args.length
    length_str = format_length(length)
    similarities = args.similarities
    bin_names = ['%s_%.1f-%.1f' % (length_str, x, y) for x, y in zip(similarities, similarities[1:] + [100.0])]
    similarities = [x / 100.0 for x in similarities]

    with common.open_possible_gzip(args.input) as inp:
        lt_filenames = create_lt_files(length, similarities, allowed_chroms, args.faidx, inp, args.output)
    bin_filenames = create_bin_files(lt_filenames, bin_names, args.faidx, args.output)

    with open(os.path.join(args.output, 'summary.csv'), 'w') as outp:
        outp.write('# %s\n' % ' '.join(sys.argv))
        outp.write('bin\tregions\tchromosomes\tsum_len\tmean_len\tmedian_len\n')

        for bin, filename in zip(bin_names, bin_filenames):
            chroms = set()
            lengths = []
            with open(filename) as inp:
                for line in inp:
                    chrom, start, end = line.strip().split('\t')
                    lengths.append(int(end) - int(start))
                    chroms.add(chrom)
            outp.write('{}\t{:,}\t{}\t{}\t{}\t{}\n'.format(bin, len(lengths), len(chroms),
                common.format_length(sum(lengths)), common.format_length(np.mean(lengths)),
                common.format_length(np.median(lengths))))

    if not args.keep:
        for filename in lt_filenames:
            os.remove(filename)


if __name__ == '__main__':
    main()
