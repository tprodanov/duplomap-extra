#!/usr/bin/env python3

import pysam
import tqdm
import argparse
import sys
import collections

import common


def gt_to_str(gt):
    if gt is None or gt[0] is None:
        return '.'
    return '/'.join(map(str, sorted(gt)))


def main():
    parser = argparse.ArgumentParser(
        description='Count PSV genotypes',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <vcf> [-d <txt>]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input vcf file with PSVs.')
    io_args.add_argument('-d', '--dbs', metavar='<file>',
        help='Optional: input list of possible databases.')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    dbs = None
    if args.dbs:
        with common.open_possible_gzip(args.dbs) as inp:
            dbs = set(map(str.strip, inp))

    inp = pysam.VariantFile(args.input)
    genotypes = collections.defaultdict(list)
    for record in tqdm.tqdm(inp):
        if dbs and record.info['db'] not in dbs:
            continue
        if record.filter.keys() != ['PASS']:
            continue
        genotypes[record.info['psv']].append(record.samples[0]['GT'])
    print('Total %d PSVs' % len(genotypes))
    print('Sum %d entries' % sum(map(len, genotypes.values())))

    counter = collections.Counter()
    for psv, gts in genotypes.items():
        if len(gts) != 2:
            if len(gts) > 2:
                sys.stderr.write('PSV %s appeared %d times\n' % (psv, len(gts)))
            continue
        gts = tuple(sorted((gt_to_str(gts[0]), gt_to_str(gts[1]))))
        counter[gts] += 1

    total_psvs = sum(counter.values())
    print('Both copies with high quality: %d PSVs' % total_psvs)
    for gts, count in counter.most_common():
        print('%s, %s:  %10d (%4.1f%%)' % (gts[0], gts[1], count, 100 * count / total_psvs))


if __name__ == '__main__':
    main()
