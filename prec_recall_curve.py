#!/usr/bin/env python3

import argparse
import re
import tqdm
import pysam
import sys
import collections
import csv
from enum import Enum

from summarize_reads import Location
import common


_NAME_PATTERN = re.compile(r'Span=([^:]+):([0-9]+)-([0-9]+)/.*_([0-9]+)$')

def load_regions(filename):
    """
    Returns dictionary { chrom_name: NonOverlappingSet }.
    """
    if filename is None:
        return None

    starts = collections.defaultdict(list)
    ends = collections.defaultdict(list)

    with common.open_possible_gzip(filename) as inp:
        for line in inp:
            chrom, start, end = line.split('\t', 3)[:3]
            starts[chrom].append(int(start))
            ends[chrom].append(int(end))

    res = {}
    for chrom in starts:
        res[chrom] = common.NonOverlappingSet(starts[chrom], ends[chrom])
    return res


class Intersection(Enum):
    Partial = 0
    Complete = 1
    Empty = 2

    def non_empty(self):
        return self != Intersection.Empty


def location_intersects(location, regions):
    """
    Returns Intersection.
    """
    if regions is None:
        return Intersection.Complete

    chrom_regions = regions.get(location.chrom)
    if chrom_regions is None:
        return Intersection.Empty

    i, j = chrom_regions.select(location.start, location.end)
    if i == j:
        return Intersection.Empty
    if i + 1 < j:
        return Intersection.Partial

    start = chrom_regions.starts[i]
    end = chrom_regions.ends[i]
    if start <= location.start and location.end <= end:
        return Intersection.Complete
    return Intersection.Partial


def analyze_reads(inp, regions, outp, covered_threshold, tails_threshold, n_simulated_reads):
    line = next(inp)
    while line.startswith('#'):
        line = next(inp)
        continue
    in_fieldnames = line.strip().split('\t')
    reader = csv.DictReader(inp, delimiter='\t', fieldnames=in_fieldnames)

    simulated_reads = 0
    mapped_reads = [0] * 255
    correct_reads = [0] * 255
    for row in tqdm.tqdm(reader):
        true_loc = Location.parse(row['true_loc'])
        true_intersects = location_intersects(true_loc, regions).non_empty()
        if true_intersects:
            simulated_reads += 1
        if row['aln_loc'] == '*':
            continue

        aln_loc = Location.parse(row['aln_loc'])
        aln_intersects = location_intersects(true_loc, regions).non_empty()
        if not true_intersects and not aln_intersects:
            continue
        # print(row['true_loc_cov'], *row['outside_tails'].split(','), sep='\t')

        mapq = int(row['mapq'])
        mapped_reads[mapq] += 1
        correctly = True
        if float(row['true_loc_cov']) < covered_threshold:
            correctly = False
        else:
            tails = tuple(map(int, row['outside_tails'].split(',')))
            if tails[0] > tails_threshold or tails[1] > tails_threshold:
                correctly = False
        if correctly:
            correct_reads[mapq] += 1

    outp.write('# %s\n' % ' '.join(sys.argv))
    if n_simulated_reads is not None and n_simulated_reads != simulated_reads:
        outp.write('# simulated reads in input file: %d\n' % simulated_reads)
        simulated_reads = n_simulated_reads
    outp.write('# simulated reads: %d\n' % simulated_reads)
    outp.write('mapq\tmapped\tcorrect\tcumul_mapped\tcumul_correct\tprecision\trecall\n')

    cumulative_mapped = 0
    cumulative_correct = 0
    for mapq in reversed(range(255)):
        cumulative_mapped += mapped_reads[mapq]
        cumulative_correct += correct_reads[mapq]
        if not cumulative_mapped:
            continue
        outp.write('%d\t%d\t%d\t' % (mapq, mapped_reads[mapq], correct_reads[mapq]))
        outp.write('%d\t%d\t' % (cumulative_mapped, cumulative_correct))
        outp.write('%.6f\t%.6f\n' % (cumulative_correct / cumulative_mapped if cumulative_mapped else 1.0,
            cumulative_correct / simulated_reads if simulated_reads else 0))


def main():
    parser = argparse.ArgumentParser(
        description='Construct Precision-Recall curve',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <csv> -o <csv> [-r <bed>]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input CSV file.')
    io_args.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output CSV file.')
    io_args.add_argument('-r', '--regions', metavar='<file>', required=False,
        help='Optional BED file: analyze only reads that intersect input regions\n'
            'with their true or aligned location.')

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('-c', '--correct', metavar=('<float>', '<int>'), nargs=2, default=[25.0, 100],
        help='Read is considered mapped correctly if its alignment location covers true location\n'
            'by at least <float> and has no outside tails longer than <int> [default: %(default)s].')
    opt_args.add_argument('-s', '--simulated-reads', metavar='<int>', type=int,
        help='Use <int> as the total number of simulated reads. If not provided, counts the number\n'
            'from input reads.')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    args = parser.parse_args()

    regions = load_regions(args.regions)
    with common.open_possible_gzip(args.input) as inp, common.open_possible_gzip(args.output, 'w') as outp:
        analyze_reads(inp, regions, outp, float(args.correct[0]), int(args.correct[1]), args.simulated_reads)


if __name__ == '__main__':
    main()
