#!/usr/bin/env python3

import pysam
import re
import os
import sys
import argparse


def rename_reads(filename, outp, hapl, read_num, subregions):
    if subregions:
        m = re.match(r'([^:]+):([0-9]+)-([0-9]+).([^.]+).sam$', os.path.basename(filename))
        if m is None:
            print('Failed to parse filename "%s"' % filename)
            exit(1)
        chrom = m.group(1)
        start = int(m.group(2)) - 1
    else:
        chrom = None
        start = 0

    inp = pysam.AlignmentFile(filename)
    for record in inp:
        if read_num % 10000 == 0:
            sys.stderr.write('Renamed {:,} reads\n'.format(read_num))
        read_chrom = record.reference_name if chrom is None else chrom
        name = 'Read_{read_num};hapl={hapl};Span={chrom}:{start}-{end}/0/0_{length}'\
            .format(read_num=read_num, hapl=hapl, chrom=read_chrom, start=start + record.reference_start + 1,
                end=start + record.reference_end, length=record.query_length)
        outp.write('@%s\n%s\n+\n%s\n' % (name, record.query_sequence, record.qual))
        read_num += 1
    return read_num


def main():
    parser = argparse.ArgumentParser(
        description='Rename reads generated using simlord',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <dir> -o <fastq>')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-a', '--reads-a', metavar='<file>', nargs='+', required=True,
        help='Multiple input sam files for reads from the first haplotype.')
    io_args.add_argument('-b', '--reads-b', metavar='<file>', nargs='*', required=True,
        help='Multiple input sam files for reads from the second haplotype.')
    parser.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output fastq file.')

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('--subregions', action='store_true',
        'Files names have form "<chrom>:<start>-<end>.<hapl>.sam".')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    read_num = 1
    outp = open(args.output, 'w')
    for i, files in enumerate([args.reads_a, args.reads_b]):
        hapl = 'ab'[i]
        for filename in files:
            read_num = rename_reads(filename, outp, hapl, read_num, args.subregions)


if __name__ == '__main__':
    main()
