#!/usr/bin/env python3

import pysam
import random
import argparse
import sys


def mutate_nt(nt, out_seq1, out_seq2):
    r = random.random()
    if nt not in 'ACGT' or r > 0.002:
        out_seq1.append(nt)
        out_seq2.append(nt)
        return
    r /= 0.002

    new_nt = random.choice('ACGT'.replace(nt, ''))
    out_seq1.append(new_nt if r < 0.75 else nt)
    out_seq2.append(new_nt if r > 0.25 else nt)


def write_header(ref, outp):
    outp.write('##fileformat=VCFv4.2\n')
    for name, length in zip(ref.references, ref.lengths):
        outp.write('##contig=<ID=%s,length=%s>\n' % (name, length))
    outp.write('##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n')
    outp.write('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tSAMPLE\n')


def write_chrom(chrom_name, chrom_seq, outp, snv_frac):
    for i, nt in enumerate(chrom_seq):
        if nt not in 'ACGT':
            continue
        r = random.random()
        if r > snv_frac:
            continue
        r /= snv_frac

        new_nt = random.choice('ACGT'.replace(nt, ''))
        gt = (r < 0.75, r > 0.25)
        outp.write('%s\t%d\t.\t%s\t%s\t100\tPASS\t.\tGT\t%d|%d\n'
            % (chrom_name, i + 1, nt, new_nt, gt[0], gt[1]))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input fasta file.')
    parser.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output vcf file.')
    parser.add_argument('-s', '--snv-frac', type=float, metavar='<float>', default=0.002,
        help='SNV fraction [%(default)s].')
    args = parser.parse_args()

    with pysam.FastaFile(args.input) as ref, open(args.output, 'w') as outp:
        write_header(ref, outp)
        for name in ref.references:
            sys.stderr.write('%s\n' % name)
            seq = ref.fetch(name)
            write_chrom(name, seq, outp, args.snv_frac)


if __name__ == '__main__':
    main()
