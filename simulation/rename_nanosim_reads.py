#!/usr/bin/env python3

import sys
import argparse


def rename_reads(inp, outp, hapl, read_num):
    for name in inp:
        if read_num % 10000 == 0:
            sys.stderr.write('Renamed {:,} reads\n'.format(read_num))
        seq = next(inp)
        name_split = name[1:].strip().split('_')
        chrom = name_split[0].replace('-', '_')
        start0 = int(name_split[1])
        length = int(name_split[6])
        outp.write('>Read_%d;hapl=%s;Span=%s:%d-%d/0/0_%d\n%s'
            % (read_num, hapl, chrom, start0 + 1, start0 + length, len(seq), seq))
        read_num += 1
    return read_num


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--reads-a', metavar='<file>', nargs='+', required=True,
        help='Multiple input fasta files for reads from the first haplotype.')
    parser.add_argument('-b', '--reads-b', metavar='<file>', nargs='*', required=True,
        help='Multiple input fasta files for reads from the second haplotype.')
    parser.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output fasta file.')
    args = parser.parse_args()

    read_num = 1
    outp = open(args.output, 'w')
    for i, files in enumerate([args.reads_a, args.reads_b]):
        hapl = 'ab'[i]
        for filename in files:
            with open(filename) as inp:
                read_num = rename_reads(inp, outp, hapl, read_num)


if __name__ == '__main__':
    main()
