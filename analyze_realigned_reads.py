#!/usr/bin/env python3

import argparse
import tqdm
import pysam
import sys

import prec_recall_curve
from summarize_reads import Location
import common


def analyze_read(read, regions, outp):
    aln_loc = Location(read.reference_name, read.reference_start, read.reference_end)
    in_regions = prec_recall_curve.location_intersects(aln_loc, regions).non_empty()
    realigned = read.has_tag('re')
    if not in_regions and not realigned:
        return

    outp.write('%s\t%d\t%s\t%d\t' % (read.query_name, len(read.query_sequence), aln_loc, read.mapping_quality))
    outp.write('%s\t%s' % ('FT'[in_regions], 'FT'[realigned]))
    if not realigned:
        outp.write('\n')
        return

    prev_loc = Location.parse(read.get_tag('oa')) if read.has_tag('oa') else aln_loc
    prev_overlap = aln_loc.overlap_perc(prev_loc)

    re_tag = read.get_tag('re').split(',')
    if len(re_tag) == 3:
        reason = re_tag[2]
    else:
        reason = re_tag[0]
    outp.write('\t%s\t%.1f\t%s\t%s\n' % (prev_loc, prev_overlap, read.get_tag('om'), reason))


def main():
    parser = argparse.ArgumentParser(
        description='Analyze Summarize reads in BAM file',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <bam> -o <csv>')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='<file>', required=True,
        help='Input realigned BAM file.')
    io_args.add_argument('-r', '--regions', metavar='<file>', required=True,
        help='Input BED file: analyze only reads that intersect input regions\n'
            'with their true or aligned location.')
    io_args.add_argument('-o', '--output', metavar='<file>', required=True,
        help='Output CSV file.')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit.')
    args = parser.parse_args()

    regions = prec_recall_curve.load_regions(args.regions)
    with pysam.AlignmentFile(args.input) as inp, common.open_possible_gzip(args.output, 'w') as outp:
        outp.write('# %s\n' % ' '.join(sys.argv))
        outp.write('name\tread_len\taln_loc\tmapq\tin_regions\trealigned\tprev_loc\tprev_overlap\tprev_mapq\treason\n')
        for read in tqdm.tqdm(inp):
            if read.flag & 0x904:
                continue
            analyze_read(read, regions, outp)


if __name__ == '__main__':
    main()
