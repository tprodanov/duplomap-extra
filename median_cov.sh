#!/bin/bash

bam_path="$1"
fai_path="$2"
cov_path="$bam_path.genomecov"
out="$bam_path.mediancov"

if ! [ -f "$cov_path" ]; then
    samtools view -bF 0x900 "$bam_path" \
        | bedtools bamtobed -i - \
        | bedtools genomecov -i - -bga -g "$fai_path" \
        > "$cov_path"
fi

 cat "$cov_path" \
    | awk '$1~/^(chr|)[0-9]+$/ {
            sum_len += $3 - $2;
            cov[$4] += $3 - $2;
        }
        END {
            acc_len = 0;
            i = -1;
            while (acc_len < sum_len / 2) {
                i += 1;
                acc_len += cov[i];
            }
            print i;
        }' | tee "$out"
